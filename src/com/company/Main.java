package com.company;

import com.Modal.Artist;

import java.util.List;
import java.util.Objects;

public class Main {

    public static final String DB_NAME = "testjava.db";
    public static final String CONNECTON_STRING = "jdbc:sqlite:C:\\Users\\sankeerthana\\IdeaProjects\\testDB\\" + DB_NAME;
    public static final String CONTACTS_TABLE = "contacts";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_EMAIL = "email";

//    public static void main(String[] args) {
//        //try (Connection con = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\sankeerthana\\IdeaProjects\\testDB\\testjava.db"); Statement statement = con.createStatement()) {
//        try {
//            Connection con = DriverManager.getConnection(CONNECTON_STRING);
////            con.setAutoCommit(false);
//            Statement statement = con.createStatement();
//            statement.execute("DROP TABLE IF  EXISTS" + CONTACTS_TABLE);
//            statement.execute("INSERT  INTO contacts(name,phone,email)" +
//                    " VALUES('Joe',777,'joe@gmail.com')");
//            statement.execute("UPDATE  contacts SET phone='9886524587' WHERE " +
//                    "name='Jhon' ");
//            statement.execute("DELETE FROM contacts WHERE name='Joe'");
//            ResultSet rs = statement.executeQuery("SELECT  * from contacts");
//
//            while (rs.next()) {
//                System.out.println(rs.getString(1) + " ," + rs.getInt(2) + ", " +
//                        rs.getString(3));
//            }
//            rs.close();
//            statement.close();
//            con.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

    public static void main(String[] args) {
        DataSource dataSource = new DataSource();
        if (!dataSource.open()) {
            System.out.println("cant Open the dataSource");
        }
        List<Artist> artistList = dataSource.queryArtists(DataSource.ORDER_BY_ASC);
        if (Objects.nonNull(artistList)) {
            artistList.forEach(System.out::println);
        }
        List<String> albumList = dataSource.queryAlbumForArtist("Iron Maiden", 1);
        if (Objects.nonNull(albumList)) {
            albumList.forEach(System.out::println);
        }
        dataSource.close();
    }
}


















