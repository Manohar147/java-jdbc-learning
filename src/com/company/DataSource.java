package com.company;

import com.Modal.Artist;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataSource {
    public static final String DB_NAME = "music.db";
    public static final String CONN_STRING = "jdbc:sqlite:C:\\Users\\sankeerthana\\IdeaProjects\\testDB\\" + DB_NAME;
    public static final String TABLE_ALBUM = "albums";
    public static final String TABLE_ALBUM_ID = "_id";
    public static final String TABLE_ALBUM_NAME = "name";
    public static final String TABLE_ALBUM_ARTIST = "artist";
    public static final int INDEX_ALBUM_ID = 1;
    public static final int INDEX_ALBUM_NAME = 2;
    public static final int INDEX_ALBUM_ARTIST = 3;

    public static final String TABLE_ARTIST = "artists";
    public static final String TABLE_ARTIST_ID = "_id";
    public static final String TABLE_ARTIST_NAME = "name";
    public static final int INDEX_ARTIST_ID = 1;
    public static final int INDEX_ARTIST_NAME = 2;

    public static final String TABLE_SONGS = "songs";
    public static final String TABLE_SONGS_ID = "_id";
    public static final String TABLE_SONGS_track = "track";
    public static final String TABLE_SONGS_title = "title";
    public static final String TABLE_SONGS_album = "album";
    public static final int INDEX_SONG_ID = 1;
    public static final int INDEX_SONG_TRACK = 2;
    public static final int INDEX_SONG_TITLE = 3;
    public static final int INDEX_SONG_ALBUM = 4;

    public static final int ORDER_BY_NONE = 1;
    public static final int ORDER_BY_ASC = 2;
    public static final int ORDER_BY_DESC = 3;

    public static final String queryAlbumsByArtist = "SELECT albums.name FROM albums INNER JOIN artists ON albums.artist=artists._id WHERE artists.name=\"Pink Floyd\" ORDER BY albums.name;";


    private Connection connection;

    public boolean open() {
        try {
            connection = DriverManager.getConnection(CONN_STRING);

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<Artist> queryArtists(int sortOrder) {
        StringBuilder sb = new StringBuilder("SELECT * FROM ");
        sb.append(TABLE_ARTIST);
        if (sortOrder != ORDER_BY_NONE) {
            sb.append(" ORDER BY ");
            sb.append(TABLE_ARTIST_NAME);
            sb.append(" COLLATE NOCASE ");
            if (sortOrder == ORDER_BY_DESC) {
                sb.append("DESC");
            } else {
                sb.append("ASC");
            }
        }
        try (Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(sb.toString())) {
            List<Artist> artistList = new ArrayList<>();
            while (resultSet.next()) {
                Artist artist = new Artist();
                artist.setId(resultSet.getInt(INDEX_ARTIST_ID));
                artist.setName(resultSet.getString(INDEX_ARTIST_NAME));
                artistList.add(artist);
            }
            return artistList;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return null;
    }

    public List<String> queryAlbumForArtist(String albumName, int sortOrder) {
        open();
        StringBuilder sb = new StringBuilder("SELECT ");
        sb.append(TABLE_ALBUM);
        sb.append(".");
        sb.append(TABLE_ALBUM_NAME);
        sb.append(" FROM ");
        sb.append(TABLE_ALBUM);
        sb.append(" INNER JOIN ");
        sb.append(TABLE_ARTIST);
        sb.append(" ON ");
        sb.append(TABLE_ALBUM);
        sb.append(".");
        sb.append(TABLE_ALBUM_ARTIST);
        sb.append(" = ");
        sb.append(TABLE_ARTIST);
        sb.append(".");
        sb.append(TABLE_ARTIST_ID);
        sb.append(" WHERE ");
        sb.append(TABLE_ARTIST + "." + TABLE_ARTIST_NAME + " = ");
        sb.append("'" + albumName + "'");
        sb.append(" ORDER BY ");
        sb.append(TABLE_ALBUM);
        sb.append(".");
        sb.append(TABLE_ALBUM_NAME);
        System.out.println(sb.toString());
        try (Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(sb.toString())) {
            List<String> albumList = new ArrayList<>();
            while (resultSet.next()) {
                albumList.add(resultSet.getString(1));
            }
            return albumList;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close();
        }
        return null;
    }


}
